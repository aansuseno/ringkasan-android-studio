# 1. Membuat Scrollable Layout
Pilih elemen ScrollView daripada layout
***
# 2. Membuat aksi button
tambahkan tag pada elemen button: ```android:onClick="namaFunction"```

function ditulis difile kotlin nya:
```
fun namaFunction(view: android.view.View) {
}
```
***

# 3. Mengambil value dari sebuah tag
```
var nama= findViewById<TextEdit>(R.id.inputNama)

var namaku = nama.text.toString()
```
```TextEdit``` merupakan nama elemen.
```inpuptNama``` id dari elemen
## Mengambill value integer
```
var nilai = findViewById<TextEdit>(R.id.inputNilai)
var skor = nilai.text.toString().toInt()
```
***
# 4. Me-set text sebuah textView
```
var pesan = findViewById<TextView>(R.id.pesanku)
pesan.apply {
    text = "Halo Semua"
}
```
***
# 5. Cara Pindah Page/Activity
```
fun pindah(view: android.view.View) {
    intent = Intent(this, KeduaActivity::class.java)
    startActivity(intent)
}
```
```KeduaActivity``` nama activity ke dua
***
# 6. Cara kirim variable antar Activity
```
fun pindah(view: android.view.View) {
    intent = Intent(this, KeduaActivity::class.java)
    intent.putExtra("haha", "hoho")
    startActivity(intent)
}
```
```haha``` adalah nama key yg akan dipanggil,
```hoho``` adalah value nya
Cara ambil value nya:
```
val haha = intent.getStringExtra("haha")
```
***
# 7. Cara Hapus Title Bar

 1. Pergi ke folder values->themes
 2. Di bagian: ```<style name="Theme.Apli" parent="Theme.MaterialComponents.DayNight.DarkActionBar">```
 3. Ganti DarkActionBar menjadi NoActionBar

***
# 8. MEmbuat class style
di folder themes tambah style baru
```
<style name="tes" parent="Theme.MaterialComponents.DayNight.NoActionBar">
    <item name="android:padding">5sp</item>
</style>
```
cara memanggil style:
```
<LinearLayout
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:layout_weight="1"
    style="@style/tes"
    android:orientation="vertical">
</LinearLayout>
```
***
# 9. Mengganti Warna button
```
android:backgroundTint="#00ff00"
```
***
# 10. Cara set nilai EditText
```
var input = findViewById<EditText>(R.id.inputan)
setText("oke")
```
***
# 11. Cara mengatasi input kosong
```
if(!input.text.toString().trim().isEmpty()) {
    nilaiBAru = input.text.toString().toInt()
}
```
***
# 12. Array di ArrayList
```
// deklarasi
var siswa = ArrayList<Array<String>>()

// menambah
siswa.add(arrayOf<String>("aa", "bb", "cc"))
siswa.add(arrayOf<String>("dd", "ee", "ff"))
siswa.add(arrayOf<String>("yy", "zz", "xx"))
siswa.add(arrayOf<String>("gg", "hh", "ii"))

// mengubah
siswa.set(1, arrayOf<String>("hoho", "haha", "hihi"))

// menghapus berdasar index
siswa.removeAt(2)

// siswa.clear() //menghapus seluruh array

// print
println(siswa[1][2])
for(i in 0 until siswa.size) {
    for(j in 0 until 3) {
        println(siswa[i][j])
    }
}
```
***
# 13. Directori folder drawable
```NamaProjek\app\src\main\res\drawable```
***
# 14. SplashScreen
```
Handler(Looper.getMainLooper()).postDelayed({
    val intent = Intent(this, HeheActivity::class.java)
    startActivity(intent)
    finish()
}, 3000)
```
3000 adalah 3 second sebelum pindah activity HeheActivity
***
# 15. Dropdown
## activity.xml :
```
<Spinner
        android:id="@+id/hoke"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:background="#FFFFFF"
        android:backgroundTint="#5A2424"
        android:entries="@array/buah"
        tools:ignore="InvalidId"
        tools:layout_editor_absoluteX="-42dp"
        tools:layout_editor_absoluteY="113dp">
</Spinner>
```
```android:entries="@array/buah"``` nama di string.xml
## res>value>string.xml :
```
<resources>
    <string name="app_name">splash</string>
    <string-array name="buah">
        <item>Apel</item>
        <item>Jeruk</item>
        <item>Nangka</item>
        <item>Manggis</item>
        <item>Semangka</item>
        <item>Pir</item>
    </string-array>
</resources>
```
## ambil nilai
```
findViewById<Spinner>(R.id.hoke).getSelectedItem().toString()
```
***
# 16. Membuat Animasi
## 1
```res``` klik kanan ```new>Drawable Resource File```. Masukkan nama file dan direktori nama anim. Oke
## 2
```
<?xml version="1.0" encoding="utf-8"?>
<set xmlns:android="http://schemas.android.com/apk/res/android">
    <translate
        android:fromXDelta="0%"
        android:fromYDelta="-100%"
        android:duration="2000"/>
    <alpha
        android:fromAlpha="0.1"
        android:toAlpha="1"
        android:duration="2000"/>
</set>
```
## 3
MainActivity.kt
```
val anim = AnimationUtils.loadAnimation(this, R.anim.animasi_keatas)
val logo = findViewById<ImageView>(R.id.logo)
logo.apply {
    animation = anim
}
```
***
# 17. Background auto crop
Harus menggunakan imageView. Dan tambahkan scaleType dng value ```centerCrop```
```
<ImageView
    android:id="@+id/imageView"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:scaleType="centerCrop"
    app:srcCompat="@drawable/bg" />
```
***
# 18. Mengambil tgl dan waktu
```
val waktu = SimpleDateFormat("yyyy-MM-dd HH:mm:ss".toString()).format(kalender.time).toString()
```
***
# 19. Format uang indoesia rupiah
```
val local = Locale("id", "ID")
val formatter = NumberFormat.getCurrencyInstance()
val totalBayar = formatter.format(bayar)
```
***
# 20. Button Action
```
val hapus = Button(this)
hapus.text = "hapus"
hapus.tag = i.toString()
hapus.setBackgroundColor(Color.parseColor("#ff0000"))
hapus.setOnClickListener(View.OnClickListener {
    riwayat.removeAt(i)
    tampilRiwayat()
})
```
***
# 21. Button icon
```
Klik kanan drawable>new>image assets>ikon type: action bar. Dibagian button tambahkan ```android:drawableRight="@drawable/ic_action_nama_ikon"
```
***
# 22. Button radius
drawable klik kanan > new > drawable Resource File > isi nama > root element: shape > oke . Isi kode berikut:
```
<corners
    android:radius="20sp"/>
```
panggil di button activity xml:
```android:background="@drawable/kotakan"```

# 23. Spinner onItemSelectedListener
```
findViewById<Spinner>(R.id.dropdown_barang).onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val barang = findViewById<Spinner>(R.id.dropdown_barang).getSelectedItem().toString()
        var harga = 0.0

        if (barang=="TV"){
            harga= 2000000.0
        }else if (barang=="Speaker"){
            harga= 750000.0
        }else if (barang=="DVD Player"){
            harga= 1250000.0
        }else if (barang=="Projector"){
            harga= 4000000.0
        }

        findViewById<EditText>(R.id.harga).setText(harga.toString())
    }

}
```
***
# 24. Ganti Garis bawah edit text
ganti saja backgroundtint
***
# 25. ArrayList ke array dropdown item
```
val adapter = ArrayAdapter<String>(
    this,
    android.R.layout.simple_spinner_dropdown_item,
    arrayNamaBarang)
adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
findViewById<Spinner>(R.id.namaBarang).adapter = adapter
```
***
